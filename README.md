## Event Displays

This package makes event displays which contain the truth jets, the reconstructed jets, and the clusters for an event.




## Inputs

The root file must contain the 4-vectors for the jets, truth jets, and clusters. For each of these objects, you can have either a vector<TLorentzVector> which contains the 4-vectors for each object in the event, or 4 separate branches for the pt, eta, phi, and m. 


## Configuration

Several things may specified in the run configuration

--fileName: The name of the file 

--treeName: The name of the tree which contains the jets and clusters

--jetName: The name of the jet collection in the root file

--truthName: The name of the truth jet collection. If none exists (or only truth studies are done), leave empty

--clusterName: The name of the cluster collection

--jetR: The radius of the jets being drawn

--isSqrtSize: Determines if the size of the constituents should be linearly proportional to pT, or square root. Square root is better at high pT

--linearPtScaling: The scaling applied to the constituent size (if drawn linearly)

--sqrtPtScaling: The scaling applied to the constituent size (if not drawn linearly)

--ptThreshold: The minimum pT of jets that are being drawn

--isTLorentz: True if the branches are in the form vector<TLorentzVector>, false if there are four inputs for each object in the form of vector<float>

--maxEta: The maximum rapidity for the event displays

--units: Set to MeV if your threshold needs to be converted to MeV (defaults to no conversion)

--labelJets: Set to 1 if you want the pT of the jets to be drawn on each plot


## How to run

First, run.sh should be changed to contain whatever configurations are necessary for your branches. 

```
make
source run.sh
```





