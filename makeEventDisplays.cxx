#ifndef eventDisplays_cxx
#define eventDisplays_cxx

#include "makeEventDisplays.h"
#include <unistd.h>
#include "TLine.h"
#include "TDirectory.h"
#include <getopt.h>


using namespace std;

eventDisplays::eventDisplays(int argc,char *argv[]){
  m_hasClusterPt = true;
  m_hasClusterM = true;
  m_hasRecoJetM = true;
  m_hasTruthJetM = true;


  gStyle->SetOptStat(0);
  gStyle->SetLabelOffset(-0.065,"X");
  gStyle->SetTitleYOffset(-1.4);
  gStyle->SetLabelOffset(-0.025,"Y");
  gStyle->SetHistLineWidth(1.5);
  gStyle->SetOptTitle(0);
  gStyle->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  gStyle->SetTitleXOffset(-1.4);

  m_threshold = 20;
  m_maxEta = 2.5;
	m_minPhi = -3.14;
	m_maxPhi = 3.14;
  m_jetName = "jeta4noalg";
  m_clusterName = "cl420";
  m_truthName = "";
  m_jetR = 0.4;
  m_fileName = "myfile_cl420_noalg_skim.root";
  m_treeName = "ntuple";
  m_isTLorentz = 1;
  m_units = "GeV";
  m_isSqrtSize = 0;
  m_sqrtPtScaling = 0.001;
  m_linearPtScaling = 0.000015;
  m_constituentColor = "#6D0A66";
  m_labelJets = 0;



	static struct option long_options[] =
	{   
    {"ptThreshold", 1, NULL, 'a'},
    {"jetR", 1, NULL, 'b'},
    {"maxEta", 1, NULL, 'c'},
    {"jetName", 1, NULL, 'd'},
    {"truthName", 1, NULL, 'e'},
    {"clusterName", 1, NULL, 'f'},
    {"fileName", 1, NULL, 'g'},
    {"isTLorentz", 1, NULL, 'h'},
    {"treeName", 1, NULL, 'i'},
    {"units", 1, NULL, 'j'},
    {"isSqrtSize", 1, NULL, 'k'},
    {"doSKGrid", 1, NULL, 'm'},
    {"skGridSize", 1, NULL, 'n'},
    {"linearPtScaling", 1, NULL, 'o'},
    {"sqrtPtScaling", 1, NULL, 'p'},
    {"labelJets", 1, NULL, 'q'},
    {NULL, 0, NULL, 0}
	};


  int opt;
  while ( (opt = getopt_long(argc, argv,"abcdefghijkopqrstuvwxyz", long_options, NULL)) != -1 ) {  // for each option...
    switch ( opt ) {
      case 'a': m_threshold = atof(optarg); break;
      case 'b': m_jetR = atof(optarg); break;
      case 'c': m_maxEta = atof(optarg); break;
      case 'd': m_jetName = optarg; break;
      case 'e': m_truthName = optarg; break;
      case 'f': m_clusterName = optarg; break;
      case 'g': m_fileName = optarg; break;
      case 'h': m_isTLorentz = atoi(optarg); break;
      case 'i': m_treeName = optarg; break;
      case 'j': m_units = optarg; break;
      case 'k': m_isSqrtSize = atoi(optarg); break;
      case 'm': m_doSKGrid = atoi(optarg); break;
      case 'n': m_skGridSize = atof(optarg); break;
      case 'o': m_linearPtScaling = atof(optarg); break;
      case 'p': m_sqrtPtScaling = atof(optarg); break;
      case 'q': m_labelJets = atoi(optarg); break;
      case 0: break;
    }
  }

  if(m_units == "MeV") m_threshold = m_threshold * 1000.;
	// We want the same scale for eta and phi
	double ratio = (m_maxPhi - m_minPhi)/(2*m_maxEta);
  c1 = new TCanvas("c1","c1", 500,500*ratio);

  if (tree == 0) {
  	TFile *file = (TFile*)gROOT->GetListOfFiles()->FindObject(m_fileName.c_str());
    if (!file || !file->IsOpen()) file = new TFile(m_fileName.c_str());
    file->GetObject(Form("%s", m_treeName.c_str()),tree);
  }
  Init(tree);

  // Make the canvas for the event display
  h_background = new TH2F(Form("h_background"), "", 100, -m_maxEta, m_maxEta, 100, m_minPhi, m_maxPhi);
  h_background->UseCurrentStyle();
  h_background->GetXaxis()->CenterTitle();
  h_background->GetYaxis()->CenterTitle();
  h_background->GetXaxis()->SetTitle("#eta");
  h_background->GetYaxis()->SetTitle("#phi");
  h_background->GetXaxis()->SetTicks("-");
  h_background->GetYaxis()->SetTicks("+");

  legJets = new TLegend(0.62, 0.945, 1.1, .99);
  legTruth = new TLegend(0.62, 0.901, 1.1, .945);
  legCluster = new TLegend(0.4, 0.901, .7, .945);

  makeLegendEntry(legCluster, Form("%s", m_clusterName.c_str()), m_constituentColor, 4, 1, 0.3);
  makeLegendEntry(legJets, Form("Anti-kt, R=%0.1f Jets", m_jetR), "#000000", 6, 7, 0);
  makeLegendEntry(legTruth,Form("Truth jets, R=%0.1f", m_jetR), "#000000", 4, 1, 0);

  c1->Print(Form("eventDisplays%s.pdf[", m_jetName.c_str()));
	Loop("");
  c1->Print(Form("eventDisplays%s.pdf]", m_jetName.c_str()));
}


void eventDisplays::Loop(string name){
  if (fChain == 0) return;
  Long64_t nentries = fChain->GetEntriesFast();

  for (Long64_t jentry = 0; jentry < nentries; jentry++) {
    fillTree(jentry);
		drawHist(Form("eventDisplays%s", m_jetName.c_str()));
  }

}


void eventDisplays::drawHist(string plotName){
	ellipses.clear();
	h_background->Draw();

  if(m_doSKGrid) drawSKGrid(clusters, plotName);
	drawObjects(clusters, 0, 0, m_constituentColor, 1, 1, 0.3, false);
	drawObjects(jets, m_threshold, m_jetR, "#000000", 6, 7, 0, m_labelJets, "reco", 0.);
	drawObjects(truthjets, m_threshold, m_jetR, "#000000", 4, 1, 0, m_labelJets, "true", 0.03);
  if(m_doSKGrid) drawSKGridLines();
	h_background->Draw("AXIS SAME");

  myText( 0.1, 0.93, 1, Form("%s", m_jetName.c_str()));

  legJets->Draw();
  legCluster->Draw();
  legTruth->Draw();

  c1->Print(Form("%s.pdf", plotName.c_str()));

	for(unsigned int i=0; i<ellipses.size(); i++) delete ellipses[i];
}

eventDisplays::~eventDisplays(){

}

void eventDisplays::drawObjects(vector<TLorentzVector> *objects, double threshold, double size, TString color, int lineWidth, int lineStyle, double transparency, bool drawPt, std::string ptName, double offset){
	for(unsigned int k=0; k<objects->size(); k++){
    double phi = (*objects)[k].Phi();
    if(phi > 3.14) phi = phi - 3.14;

    if(abs((*objects)[k].Eta()) > m_maxEta) continue;
    if((*objects)[k].Pt() < threshold) continue;
    
    double radius = 0;
    if(size > 0) radius = size;
    else {
      if(m_isSqrtSize){
        radius = sqrt((*objects)[k].Pt())*m_sqrtPtScaling;
      }
      else{
        radius = (*objects)[k].Pt()*0.015/10.0/10.0/10.0;
      }
    }

    
    
    TEllipse *jet = new TEllipse((*objects)[k].Eta(), phi, radius, radius);
    jet->SetLineColor(TColor::GetColor(color));
    jet->SetFillColorAlpha(TColor::GetColor(color), transparency);
    jet->SetLineWidth(lineWidth);
    jet->SetLineStyle(lineStyle);
    ellipses.push_back(jet); // Add to a vector so that you can delete them later
    double pi = 3.14159;
    double eta = (*objects)[k].Eta();
    double etaFrac = 0.11 + (0.4 + eta+m_maxEta)/(2*m_maxEta) * 0.8;
    double phiFrac = 0.1 + (phi+pi) / (2*pi) * 0.8 + offset;
    double pt = (*objects)[k].Pt() / 1000.;
    if(drawPt) myText(etaFrac, phiFrac, 1, Form("p_{T}^{%s} = %.0f", ptName.c_str(), pt));

    jet->Draw("SAME");
  } 
}


void eventDisplays::drawSKGrid(std::vector<TLorentzVector> *clusters, string plotName){
  std::vector<TLine*> gridSK;
  gridSK.clear();

  const Int_t NRGBs = 2;
  const Int_t NCont = 9;
  Double_t stops[NRGBs] = { .00, 1.0};
  Double_t red[NRGBs]   = { .80, .93};
  Double_t green[NRGBs] = { .80, .93};
  Double_t blue[NRGBs]  = { .80, .93};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);

  int nphiSK = int (6.28 / m_skGridSize + 0.5);
  int netaSK = int (2*m_maxEta / m_skGridSize + 0.5);


  int nbinsx = netaSK;
  int nbinsy = nphiSK;
  TH2F *h_grid = new TH2F(Form("h_clone_%s", plotName.c_str()), "", nbinsx, -m_maxEta, m_maxEta, nbinsy, m_minPhi, m_maxPhi);

  for(unsigned int i=0; i<clusters->size(); i++){
    h_grid->Fill((*clusters)[i].Eta(), (*clusters)[i].Phi());
  }
  for(int i=0; i<h_grid->GetNbinsX(); i++){
    for(int j=0; j<h_grid->GetNbinsY(); j++){
      if(h_grid->GetBinContent(i+1, j+1) > 0)
        h_grid->SetBinContent(i+1, j+1, 1);
    }
  }
  h_grid->GetZaxis()->SetRangeUser(0,1);

  h_grid->Draw("COL SAME");


}

void eventDisplays::drawSKGridLines(){
  std::vector<TLine*> gridSK;
  gridSK.clear();

  TLine *l;
  int nphiSK = int (6.28 / m_skGridSize + 0.5);
  double dphiSK = 6.28/ nphiSK;
  int netaSK = int (2*m_maxEta / m_skGridSize + 0.5);
  double detaSK = (2*m_maxEta)/ netaSK;


  for(double i=-m_maxEta; i<m_maxEta; i+= detaSK){
    l = new TLine(i, m_minPhi,i, m_maxPhi);
    l->SetLineColor(kGray);
    gridSK.push_back(l);
  }

  for(double i= m_minPhi; i<m_maxPhi; i+= dphiSK){
    l = new TLine(-m_maxEta, i, m_maxEta,i);
    l->SetLineColor(kGray);
    gridSK.push_back(l);
  }

  for(unsigned int k=0; k<gridSK.size(); k++){
    gridSK[k]->Draw();
  }


}

void eventDisplays::makeLegendEntry(TLegend *leg, string title, TString color, int lineWidth, int lineStyle, double transparency){
  TEllipse *legendEllipse = new TEllipse(0, 0, 0.1, 0.1);
  legendEllipse->SetFillColorAlpha(TColor::GetColor(color), transparency);
 	legendEllipse->SetLineColor(TColor::GetColor(color));
	legendEllipse->SetLineWidth(lineWidth);
	legendEllipse->SetLineStyle(lineStyle);
  leg->AddEntry(legendEllipse, Form("%s", title.c_str()), "f");
}


void eventDisplays::fillTree(Long64_t jentry){
	if (fChain == 0) return;
	truthjets = 0;
	jets = 0;
	clusters = 0;

	if(m_isTLorentz){
    if(m_truthName  != ""){
	    b_truthjets->GetEntry(jentry);
    }
  	b_jets->GetEntry(jentry);
	  b_clusters->GetEntry(jentry);
	}
	else{
		jets = new vector<TLorentzVector>;
		truthjets = new vector<TLorentzVector>;
		clusters = new vector<TLorentzVector>;

    if(m_truthName  != ""){
      b_truthjets_pt->GetEntry(jentry);
      b_truthjets_eta->GetEntry(jentry);
      b_truthjets_phi->GetEntry(jentry);
      if(m_hasTruthJetM) b_truthjets_m->GetEntry(jentry);
    }

    b_jets_pt->GetEntry(jentry);
    b_jets_eta->GetEntry(jentry);
    b_jets_phi->GetEntry(jentry);
    if(m_hasRecoJetM) b_jets_m->GetEntry(jentry);

    b_clusters_pt->GetEntry(jentry);
    b_clusters_eta->GetEntry(jentry);
    b_clusters_phi->GetEntry(jentry);
    if(m_hasClusterM) b_clusters_m->GetEntry(jentry);

    for(unsigned int i=0; i<jets_pt->size(); i++){
      TLorentzVector jet(0,0,0,0);
      if(m_hasRecoJetM) jet.SetPtEtaPhiM( (*jets_pt)[i], (*jets_eta)[i], (*jets_phi)[i], (*jets_m)[i]);
      else jet.SetPtEtaPhiM( (*jets_pt)[i], (*jets_eta)[i], (*jets_phi)[i], 0);
      jets->push_back(jet);
    }

    
    if(m_truthName  != ""){
      for(unsigned int i=0; i<truthjets_pt->size(); i++){
        TLorentzVector truth(0,0,0,0);
        if(m_hasTruthJetM) truth.SetPtEtaPhiM( (*truthjets_pt)[i], (*truthjets_eta)[i], (*truthjets_phi)[i], (*truthjets_m)[i]);
        else truth.SetPtEtaPhiM( (*truthjets_pt)[i], (*truthjets_eta)[i], (*truthjets_phi)[i], 0);
        truthjets->push_back(truth);
      }
    }

		for(unsigned int i=0; i<clusters_pt->size(); i++){
			TLorentzVector cl(0,0,0,0);
			if(m_hasClusterM) cl.SetPtEtaPhiM( (*clusters_pt)[i], (*clusters_eta)[i], (*clusters_phi)[i], (*clusters_m)[i]);
			else cl.SetPtEtaPhiM( (*clusters_pt)[i], (*clusters_eta)[i], (*clusters_phi)[i], 0);
			clusters->push_back(cl);
		}
	}
}


#endif
