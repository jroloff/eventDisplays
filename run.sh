./main \
	--jetName jeta4sk \
	--truthName AntiKt4TruthJets \
	--clusterName cl420 \
	--ptThreshold 20 \
	--fileName myfile_cl420_sk_skim.root \
	--maxEta 2.5 \
  --doSK 1 \
  --skGridSize 0.6 \
  --treeName ntuple \
  --units MeV \
  --labelJets 0 \
  --isSqrtSize 1 \
	--isTLorentz 0

