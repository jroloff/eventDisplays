#include "TTree.h"
#include "TBranch.h"
#include "TLorentzVector.h"
#include "TStyle.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include "TFile.h"
#include "TROOT.h"
#include "TChain.h"
#include "TH2.h"

#include "TString.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TEllipse.h"
#include "TColor.h"

#include "AtlasUtils.h"
#include "AtlasLabels.h"


using namespace std;

#ifndef eventDisplays_H
#define eventDisplays_H

#ifdef __CINT__
#pragma link C++ class vector<TLorentzVector>+;
#endif

class eventDisplays{

private:
	TTree          *fChain;   //!pointer to the analyzed TTree or TChain

  double m_skGridSize;
  int m_doSKGrid;

  bool m_hasClusterPt;
  bool m_hasClusterM;
  bool m_hasRecoJetM;;
  bool m_hasTruthJetM;

  double m_minPhi;
  double m_maxPhi;
	double m_maxEta; // Maximum rapidity for event display
	double m_jetR; // Radius of jets
	string m_jetName; // Name of jets in file
	string m_clusterName; // Name of clusters in file
	string m_truthName; // Name of truth jets in file
  string m_constituentColor; // A hex string with the color for the constituents
	double m_threshold; // Minimum jet pT that you wish to plot
	string m_fileName; // Name of file with events
	int m_isTLorentz; // Are the inputs TLorentzVectors?
  string m_units; // The units (GeV, MeV) of the inputs
	string m_treeName;
  int m_isSqrtSize;
  double m_linearPtScaling;
  double m_sqrtPtScaling;
  int m_labelJets;
  

	// An empty histogram over which to draw the jets and clusters
	TH2F* h_background;

	TBranch* b_truthjets;//!
	std::vector<TLorentzVector> *truthjets;//!
	TBranch* b_truthjets_pt;//!
	std::vector<float> *truthjets_pt;//!
	TBranch* b_truthjets_eta;//!
	std::vector<float> *truthjets_eta;//!
	TBranch* b_truthjets_phi;//!
	std::vector<float> *truthjets_phi;//!
	TBranch* b_truthjets_m;//!
	std::vector<float> *truthjets_m;//!

  TBranch* b_jets;//!
  vector<TLorentzVector>* jets;//!
  TBranch* b_jets_pt;//!
  vector<float>* jets_pt;//!
  TBranch* b_jets_eta;//!
  vector<float>* jets_eta;//!
  TBranch* b_jets_phi;//!
  vector<float>* jets_phi;//!
  TBranch* b_jets_m;//!
  vector<float>* jets_m;//!

  TBranch* b_clusters;//!
  vector<TLorentzVector>* clusters;//!
  TBranch* b_clusters_pt;//!
  vector<float>* clusters_pt;//!
  TBranch* b_clusters_eta;//!
  vector<float>* clusters_eta;//!
  TBranch* b_clusters_phi;//!
  vector<float>* clusters_phi;//!
  TBranch* b_clusters_m;//!
  vector<float>* clusters_m;//!

  vector<TEllipse*> ellipses;
	
	// The file with the jets and clusters
  TTree *tree;

  TLegend *legTruth;
	TLegend *legJets;
	TLegend *legCluster;

  TCanvas *c1;

	void drawHist(string plotName);
	void makeLegendEntry(TLegend *leg, string title, TString color, int lineWidth, int lineStyle, double transparency);
	void drawObjects(vector<TLorentzVector> *objects, double threshold, double size, TString color, int lineWidth, int lineStyle, double transparency = 0, bool drawPt = false, string ptName="", double offset = 0.);

  void drawSKGrid(std::vector<TLorentzVector> *clusters, string plotName);
  void drawSKGridLines();

	void fillTree(Long64_t jentry);
	void Loop(string name);
	virtual void Init(TTree *tree);

public:

	eventDisplays(int argc,char *argv[]);
	~eventDisplays();


};

#endif


#ifdef eventDisplays_cxx

void eventDisplays::Init(TTree *tree){
	jets = 0;
	jets_pt = 0;
	jets_eta = 0;
	jets_phi = 0;
	jets_m = 0;

	truthjets = 0;
	truthjets_pt = 0;
	truthjets_eta = 0;
	truthjets_phi = 0;
	truthjets_m = 0;

	clusters = 0;
	clusters_pt = 0;
	clusters_eta = 0;
	clusters_phi = 0;
	clusters_m = 0;

	b_jets = new TBranch();
	b_jets_pt = new TBranch();
	b_jets_eta = new TBranch();
	b_jets_phi = new TBranch();
	b_jets_m = new TBranch();

	b_truthjets = new TBranch();
	b_truthjets_pt = new TBranch();
	b_truthjets_eta = new TBranch();
	b_truthjets_phi = new TBranch();
	b_truthjets_m = new TBranch();

	b_clusters = new TBranch();
	b_clusters_pt = new TBranch();
	b_clusters_eta = new TBranch();
	b_clusters_phi = new TBranch();
	b_clusters_m = new TBranch();
	
	gROOT->ProcessLine(".L loader.C+");

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
	if(m_isTLorentz){
	  fChain->SetBranchAddress(Form("%s", m_truthName.c_str()), &truthjets, &b_truthjets);
  	fChain->SetBranchAddress(Form("%s", m_jetName.c_str()), &jets, &b_jets);
	  fChain->SetBranchAddress(Form("%s", m_clusterName.c_str()), &clusters, &b_clusters);
	}
	else{
    if(m_truthName != ""){
      fChain->SetBranchAddress(Form("%s_pt", m_truthName.c_str()), &truthjets_pt, &b_truthjets_pt);
      fChain->SetBranchAddress(Form("%s_eta", m_truthName.c_str()), &truthjets_eta, &b_truthjets_eta);
      fChain->SetBranchAddress(Form("%s_phi", m_truthName.c_str()), &truthjets_phi, &b_truthjets_phi);
      m_hasTruthJetM = !fChain->SetBranchAddress(Form("%s_m", m_truthName.c_str()), &truthjets_m, &b_truthjets_m);
    }

    fChain->SetBranchAddress(Form("%s_pt", m_jetName.c_str()), &jets_pt, &b_jets_pt);
    fChain->SetBranchAddress(Form("%s_eta", m_jetName.c_str()), &jets_eta, &b_jets_eta);
    fChain->SetBranchAddress(Form("%s_phi", m_jetName.c_str()), &jets_phi, &b_jets_phi);
    m_hasRecoJetM = !fChain->SetBranchAddress(Form("%s_m", m_jetName.c_str()), &jets_m, &b_jets_m);

    m_hasClusterPt = fChain->SetBranchAddress(Form("%s_pt", m_clusterName.c_str()), &clusters_pt, &b_clusters_pt);
    if(m_hasClusterPt)  fChain->SetBranchAddress(Form("%s_et", m_clusterName.c_str()), &clusters_pt, &b_clusters_pt);
    fChain->SetBranchAddress(Form("%s_eta", m_clusterName.c_str()), &clusters_eta, &b_clusters_eta);
    fChain->SetBranchAddress(Form("%s_phi", m_clusterName.c_str()), &clusters_phi, &b_clusters_phi);
    m_hasClusterM = !fChain->SetBranchAddress(Form("%s_m", m_clusterName.c_str()), &clusters_m, &b_clusters_m);
	}
}

#endif // #ifdef eventDisplays_cxx



