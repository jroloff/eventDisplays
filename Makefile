CC = g++
CFLAGS = -c -g -Wall `root-config --cflags`
LDFLATS =`root-config --glibs`
GLIBS = $(shell root-config --glibs)

ATLASOBJS = AtlasLabels.o AtlasUtils.o 
RootClass=myClass.h
RootDict=Dict.cpp
RootLinkDef=LinkDef.h 
INC = -l


main : runEventDisplays.o makeEventDisplays.o $(ATLASOBJS)
	$(CC)  $(LDFLATS) runEventDisplays.o makeEventDisplays.o $(ATLASOBJS) -o main

AtlasUtils.o: AtlasUtils.C
	$(CC) $(CFLAGS) AtlasUtils.C $<

AtlasLabels.o: AtlasLabels.C
	$(CC) $(CFLAGS) AtlasLabels.C $<

runEventDisplays.o: runEventDisplays.cxx $<
	$(CC) $(CFLAGS) runEventDisplays.cxx

makeEventDisplays.o: makeEventDisplays.cxx $<
	$(CC) $(CFLAGS) makeEventDisplays.cxx

$(RootDict): $(RootClass) $(RootLinkDef)
	rootcint -v4 -f $(RootDict) -c $(INC) $(RootClass) $(RootLinkDef)

clean:
	rm *o main


